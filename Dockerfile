FROM docker.io/liyehaha/py3-venv:latest

ENV proj=prod
ENV LANG en_US.UTF-8
ENV app_key ''
ENV app_secret ''
ENV host ''
ENV port ''

ADD ./supervisord.conf /etc/supervisord.conf

RUN mkdir -p /home/works/program/

ADD . /home/works/program/
RUN /home/works/python3/bin/pip3.6 install -r /home/works/program/requirements.txt  -i http://pypi.douban.com/simple/ --trusted-host pypi.douban.com

RUN mkdir -p /home/works/supervisor/logs

EXPOSE 8000

RUN chown works.works -R /home/works/
RUN yum clean all

CMD ["supervisord", "-c", "/etc/supervisord.conf"]
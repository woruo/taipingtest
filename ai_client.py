# -*- coding: UTF-8 -*-
import hashlib
import time
import json
import uuid
import requests
import aiohttp
import config

try:
    import wulai_logger as logger
except ImportError:
    import logging as logger

HOST = config.wul_api_host
KEY = config.app_key
SECRET = config.app_secret


def get_headers():
    pubkey = KEY
    secret = SECRET
    timestamp = str(int(time.time()))
    nonce = uuid.uuid4().hex
    sign = hashlib.sha1(
        (nonce + timestamp + secret).encode("utf-8")).hexdigest()
    data = {
        "pubkey": pubkey,
        "sign": sign,
        "nonce": nonce,
        "timestamp": timestamp
    }
    headers = {}
    for k, v in data.items():
        headers["Api-Auth-" + k] = v
    return headers


async def send_request(url, data):
    headers = get_headers()
    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.post(url, json=data) as resp:
            result = ''
            try:
                text = await resp.text()
                result = json.loads(text)
            except Exception:
                print('text', text)
            return result


async def create_user(nickname, username, img_url=''):
    """
    创建用户
    :return:
    """
    user_data = {
        "imgurl": img_url,
        "nickname": nickname,
        "username": username
    }
    url = HOST + "/v1/user/create"
    return await send_request(url, user_data)


async def get_bot_response(data, user_id):
    """
    发送用户数据
    :param data:
    :param user_id:
    :return:
    """
    data = {
        "msg_body": {
            "text": {
                "content": data,
                "tts": "",
                "tts_type": "AMR"
            },

        },
        "user_id": user_id
    }

    url = HOST + "/v1/msg/bot-response"
    return await send_request(url, data)


async def send_user_msg(data, user_id):
    """
    发送用户数据
    :param data:
    :param user_id:
    :return:
    """
    data = {
        "msg_body": {
            "text": {
                "content": data,
                "tts": "",
                "tts_type": "AMR"
            },

        },
        "third_msg_id": "",
        "user_id": user_id
    }

    url = HOST + "/v1/msg/receive"
    return await send_request(url, data)


async def get_msg_history(user_id):
    """
    获取历史
    :return:
    """
    post_data = {
        "direction": "BACKWARD",
        "msg_id": "",
        "num": 10,
        "user_id": user_id
    }
    url = HOST + "/v1/msg/history"
    return await send_request(url, post_data)


async def get_inner_bot_response(data):
    url = HOST + '/v1/msg/inner/bot-response'
    msg = data.get('msg', '')
    extra = data.get("extra", '')
    user_id = data.get('user_id')
    sendobj = {
        'msg_body': {
            'text': {
                'content': msg,
            }
        },
        'extra': extra,
        'user_id': user_id,
        'username': user_id,
    }

    return await send_request(url, sendobj)


if __name__ == "__main__":
    pass

var host = "http://"+host_name+ ":" + port,
    ws_host = "ws://"+host_name+ ":" + port;
// var host = "http://127.0.0.1:8000";
//   var ws_host = "ws://127.0.0.1:8000";



  if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
    host = "http://127.0.0.1:" + port;
    ws_host = "ws://127.0.0.1:" + port;
  }

  function htmlspecialchars(str) {
    str = str || '';
    str = str.replace(/&/g, '&amp;');
    str = str.replace(/</g, '&lt;');
    str = str.replace(/>/g, '&gt;');
    str = str.replace(/"/g, '&quot;');
    str = str.replace(/'/g, '&#039;');
    str = str.replace(/\n/g, '<br>');
    var urlPattern = /\b(?:https?|http):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/gim;
    str = str.replace(urlPattern, '<a href="$&" target="_blank">$&</a>');
    return str;
  }

  function formatYYYYMMDD(d) {
    var month = '' + (d.getMonth() + 1);
    var day = '' + d.getDate();
    var year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    var cur_day = formatYYYYMMDD(new Date());
    if (cur_day != formatYYYYMMDD(date)) {
      strTime = cur_day + ' ' + strTime;
    }
    return strTime;
  }

  var WuLai = function(params) {
    this.ws = new WebSocket(ws_host + '/v1/chat');
    this.ready = false;
    this.name = "学旅家";
    this.autoOpen = false;
    this.fullScreen = false;
    this.serviceName = '';

    this.parseParams(params);
    this.startup();
    this.init();
  };

  WuLai.prototype.parseParams = function(params) {
    this.last_time = new Date();
    if (params && params.userId) {
      this.userId = params.userId;
    } else {
      this.userId = $.cookie('wulai_userid');
    }
    if (!this.userId) {
      this.userId = "mbjy_" + Math.random().toString(36).substr(2) + this.last_time.getTime();
    }
    if (params && params.autoOpen != undefined) {
      this.autoOpen = params.autoOpen;
    }
    if (params && params.fullScreen != undefined) {
      this.fullScreen = params.fullScreen;
    }
    if (params && params.userInfo) {
      this.userInfo = params.userInfo;
    }
  };

  WuLai.prototype.init = function() {
    this.updateUI(this.autoOpen, this.fullScreen);
    this.bindEvent();

    this.addInfoLog({
      msg: '连接中，请稍等...'
    });
  };

  WuLai.prototype.updateUI = function(autoOpen, fullScreen) {
    if (!autoOpen) {
      $('.wulai').hide();
    }

    if (/(iPhone|iPad|iPod|iOS|Android)/i.test(navigator.userAgent)) {
      $('.wulai').css({
        'height': $(window).height()
      });
    }

    if (fullScreen) {
      $('.wulai').css('width', '100%');
      $('.wulai').css('right', '0');
      $('.wulai').css({
        'height': $(window).height()
      });
    }
  }

  WuLai.prototype.setServiceName = function(name) {
    this.serviceName = name;
  }

  WuLai.prototype.open = function(params) {
    var self = this;
    if (!params) {
      params = {
        userId: self.userId
      };
    }
    this.parseParams(params);
    this.updateUI(this.autoOpen, this.fullScreen);
    if (self.ws && self.ws.readyState === 1) {
      self.ws.send(JSON.stringify({
        action: 'login',
        user_id: self.userId,
        user_info: self.userInfo
      }));
    }
    $(".wulai").show();
  };

  WuLai.prototype.startup = function() {
    var xtpl = [
      '<link rel="stylesheet" href="' + host + '/static/css/wulai_sdk.css"/>',
      '<div class="wulai" style="visibility:hidden">',
      '<div class="wulai-info">',
      '<div class="wulai-title"><img src="' + host + '/static/img/live-chat-bot.svg">',
      '<span class="title-text">自助服务</span>',
      '<span class="minimize"><div class="min_icon"></div></span>',
      '<span class="fold"></span></div>',
      '</div>',
      '<div class="wulai-pannel-bd">',
      '<div class="wulai-item current" data-id="group">',
      '</div>',
      '</div>',
      '<div class="wulai-pannel-ft"><textarea type="text" class="wulai-input" placeholder="请输入..."></textarea><span class="wulai-send-btn">发送</span></div>',
      '<div class="wulai-help">来也提供服务支持</div>',
      '</div>'
    ].join('\n');
    $('html').append(xtpl);
  }


  WuLai.prototype.checkRobot = function() {
    var i = 0;
    while (i++ < 1E3) {
      clearInterval(i);
    }
    if (document.visibilityState && document.visibilityState !== 'visible') {
      return false;
    }
    return true;
  };


  WuLai.prototype.bindEvent = function() {
    var self = this;

    self.ws.onopen = function(event) {

      if (!self.checkRobot()) return;
      if (self.ws && self.ws.readyState === 1) {
        self.ws.send(JSON.stringify({
          action: 'login',
          user_id: self.userId,
          user_info: self.userInfo
        }));
      }
    }

    self.ws.onmessage = function(event) {
      result = JSON.parse(event.data);
      if (result.action === 'login' && result.ok) {
        $(".wulai-log-info").hide();
        self.ready = true;
        $.cookie('wulai_userid', self.userId);
        for (var i = 0; i < result.history.length; i++) {
          self.addChatLog(result.history[i]);
        }
        self.addTimeLog(new Date());
      } else {
        self.addChatLog({
          msg: result['data'],
          time: new Date()
        })
      }
    }

    self.ws.onerror = function(event) {
      self.addInfoLog({
        msg: '连接已断开，请刷新页面重新登录'
      });
    }

    self.ws.onclose = function(event) {
      self.addInfoLog({
        msg: '您已离线，请刷新页面重新登录'
      });
    }


    $('.wulai').on('keydown', function(evt) {
      if (evt.keyCode == 27) {
        $(this).addClass('wulai-fold');
      }
    });


    $('.wulai-input').on('keydown', function(evt) {
      var $this = $(this);
      if (evt.keyCode == '13' && $.trim($this.val()) || evt.isTrigger) {
        var val = $this.val();
        if (val.length >= 512) {
          val = val.slice(0, 500) + '...';
        }
        if (!self.checkRobot()) return;
        if (self.ws && self.ws.readyState === 1) {
          var data = {
            user_id: self.userId,
            msg: val,
            time: new Date(),
            action: "query",
            is_self: true
          };
          self.ws.send(JSON.stringify(data));
        }
        self.addChatLog(data);
        $this.val('').focus();
        return false;
      }
    });
    $('.wulai-send-btn').on('click', function(evt) {
      if ($.trim($('.wulai-input').val())) {
        $('.wulai-input').trigger('keydown');
        $('.wulai-input').trigger('keyup');
      }
    });

    $(".minimize").on('click', function(evt) {
      evt.preventDefault();
      $('.wulai').hide();
    });
  };


  WuLai.prototype.checkOnline = function() {
    if (!this.ready) {
      this.addInfoLog({
        msg: '连接失败，请刷新页面重新登录'
      });
      return false;
    }
    if (this.ws && this.ws.readyState != 1) {
      this.addInfoLog({
        msg: '您已离线，请刷新页面重新登录'
      });
      return false;
    }
    return true;
  };

  WuLai.prototype.addChatLog = function(data) {
    if (!this.checkOnline()) return;

    var logXtpl = [
      '<div class="wulai-log' + (data.is_self ? ' myself' : '') + '">',
      '<div class="time"' + (data.is_self ? ' myself' : '') + '>' + (data.is_self ? '' : this.serviceName + ' '),
      formatAMPM(new Date(data.time)) + '</div>',
      '<span class="detail"><% msg %></span>',
      '</div>'
    ];
    var $log = logXtpl.join('\n').replace(/<%\s*?(\w+)\s*?%>/gm, function($0, $1) {
      return htmlspecialchars(data && data[$1] || '');
    });
    var $target = $(".wulai-item.current");
    if (data.time) {
      this.addTimeLog(new Date(data.time));
    } else {
      this.checkTimeLog();
    }
    $target.append($log);
    this.scroll(data.is_self);
    this.last_time = new Date();
  };

  WuLai.prototype.scroll = function(isSelf) {
    var $target = $(".wulai-item");
    var $box = $('.wulai-pannel-bd');
    var H = $target.height();
    var DELTA = 300;
    if (isSelf || $box.scrollTop() < H - DELTA) {
      $box.scrollTop(H);
      $target.attr('data-lastscroll', H);
    }
  }

  WuLai.prototype.addInfoLog = function(data) {
    $(".wulai-log-info").hide();
    var $info = '<div class="wulai-log-info">' + htmlspecialchars(data.msg) + '</div>';
    var $target = $(".wulai-item.current");
    $target.append($info);
    this.scroll();
  };


  WuLai.prototype.checkTimeLog = function() {
    var cur_time = new Date();
    var diff_mins = Math.round(cur_time.getTime() - this.last_time.getTime());
    if (diff_mins > 5 * 60 * 1000) {
      this.addTimeLog(cur_time);
    }
  };


  WuLai.prototype.addTimeLog = function(t) {
    //var $info = '<div class="wulai-log-info wulai-log-time">' + formatAMPM(t) + '</div>';
    //var $target = $(".wulai-item.current");
    //$target.append($info);
    //this.scroll();
  };
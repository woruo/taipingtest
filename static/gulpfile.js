var gulp = require('gulp');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('build', function () {
  return gulp.src(['js/jquery.min.js', 'js/jquery.cookie.js', 'js/sdk.js'])
    .pipe(jshint())
    .pipe(uglify())
    .pipe(concat('wulai.min.js'))
    .pipe(gulp.dest('build'));
});
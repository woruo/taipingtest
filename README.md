### WeHook项目模板

**使用方法**

1. git clone git@gitlab.com:laiye-shanghai-tech/tornado_template.git new_project(项目名)；
2. app.py是Tornado的实现文件，wulai_api.py是吾来平台接口的python实现，不需要的可以忽略或删除；
3. 更新webhook_handler.py，实现项目代码, 当前是原样返回，不作处理；
4. wulai_logger.py用于输出日志；
5. supervisord.conf是监控进程supervisor的配置文件，主要需要修改以下部分:

    ```
    [program:tornado_template]                                                          ; 修改tornado_template为新项目名
    user=works                                                                          ; 启动用户，一般无需修改
    command=/home/works/python3/bin/python app.py                                       ; 修改为新项目的启动命令
    numprocs=1                                                                          ; 启动多少个该程序
    directory=/home/works/tornado_template                                              ; 修改新项目路径
    autostart=true
    autorestart=true
    killasgroup=true
    stopasgroup=true
    stdout_logfile=/home/works/supervisor/logs/tornado_template.log  ; 修改项目日志文件
    stdout_logfile_maxbytes=500MB
    stdout_logfile_backups=8
    stderr_logfile=/home/works/supervisor/logs/tornado_template.error.log ; 修改错误日志文件
    stderr_logfile_maxbytes=500MB
    stderr_logfile_backups=8
    ```

6. 更新Dockerfile

    ```
    ... (上面的无需改动)

    （创建项目文件夹，更新tornado_template为新项目名称）
    RUN mkdir -p /home/works/tornado_template/

    ADD . /home/works/tornado_template/

    (安装python依赖包)
    RUN /home/works/python3/bin/pip3.6 install -r /home/works/tornado_template/requirements.txt

    (创建日志文件夹)
    RUN mkdir -p /home/works/supervisor/logs

    (开放端口)
    EXPOSE 9993

    (下面的无需修改)
    RUN chown works.works -R /home/works/
    RUN yum clean all

    CMD ["supervisord", "-c", "/etc/supervisord.conf"]
    ```

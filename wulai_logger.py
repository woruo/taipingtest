# -*- coding: UTF-8 -*-
import os
import json
import logging
import config

# 日志格式管理
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

# 主要使用的logger
logger = logging.getLogger("wulai_logger")
logger.setLevel(config.log_level)
normal_level = config.log_level

# 日志存放路径
normal_log_path = config.normal_log_path
warn_log_path = config.warn_log_path
error_log_path = config.error_log_path

###############################################################################
# 一般日志存放地址
wulai_loghd_info = logging.FileHandler(normal_log_path, encoding='utf-8')
wulai_loghd_info.setFormatter(formatter)
wulai_loghd_info.setLevel(normal_level)

# 警告日志，但文件存放，考虑到，应用存在优化空间，单独存放warn提供优化思路和提前预知隐患bug
wulai_logger_single_warn = logging.getLogger("wulai_logger_single_warn")
wulai_logger_single_warn.setLevel(logging.WARN)
wulai_loghd_single_warn = logging.FileHandler(warn_log_path, encoding='utf-8')
wulai_loghd_single_warn.setFormatter(formatter)
wulai_loghd_single_warn.setLevel(logging.WARN)

# 错误级别以上的日志单独存放，方便查找问题
wulai_loghd_error = logging.FileHandler(error_log_path, encoding='utf-8')
wulai_loghd_error.setFormatter(formatter)
wulai_loghd_error.setLevel(logging.ERROR)
###############################################################################

# handler 管理
logger.addHandler(wulai_loghd_info)
logger.addHandler(wulai_loghd_error)
wulai_logger_single_warn.addHandler(wulai_loghd_single_warn)


def debug(msg, *args, **kwargs):
    if config.debug:
        print(msg)
    logger.debug(msg, *args, **kwargs)


def info(msg, *args, **kwargs):
    if config.debug:
        print(msg)
    logger.info(msg, *args, **kwargs)


def error(msg, *args, **kwargs):
    if config.debug:
        print(msg)
    logger.error(msg, *args, **kwargs)


# warning 之所以除了记录在正常的runing日志之外还单独存一份warning的日志是为了能定期查看waning
# 提前预知warn是不是异常多，提前预知隐患


def warning(msg, *args, **kwargs):
    logger.warning(msg, *args, **kwargs)
    wulai_logger_single_warn.warning(msg, *args, **kwargs)


# 需要讲log_level 设置为DEBUG才会打印，因为如果不是DEBUG没必要pretty_json
def pretty_json(json_msg, *args, **kwargs):
    data = json.dumps(json_msg, indent=4, sort_keys=True, ensure_ascii=False)
    logger.debug(data, *args, **kwargs)


if __name__ == "__main__":
    debug("debug")
    info("info")
    error("error")
    warning("warning")

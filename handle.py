import json
import random
import asyncio
import traceback
import urllib.parse
import concurrent.futures
import tornado
import tornado.web
import tornado.websocket
from user_agents import parse
import alarm
import webhook
import config
import ai_client

try:
    import wulai_logger as logger
except ImportError:
    import logging as logger


class BaseHandler(tornado.web.RequestHandler):
    TASK_POOL = concurrent.futures.ProcessPoolExecutor()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.body = {}

    def add_cors(self):
        """
        允许任意跨域
        """
        self.set_header('Access-Control-Allow-Headers', '*')
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header('Access-Control-Allow-Methods', '*')

    def parse_arguments(self):
        """
        对表单参数和 json 参数的兼容
        """
        try:
            content_type = self.request.headers.get('Content-Type', '')
            if content_type.startswith('application/json'):
                self.body = json.loads(self.request.body)
            elif content_type.startswith('application/x-www-form-urlencoded'):
                body = urllib.parse.parse_qsl(self.request.body.decode())
                self.body = {key: val for key, val in body}
        except Exception:
            msg = traceback.format_exc()
            logger.warning(msg)

    async def prepare(self):
        """
        每个请求处理跨域和参数解析
        """
        self.add_cors()
        self.parse_arguments()

    def write_error(self, status_code, **kwargs):
        """
        异常处理
        :parapm status_code: http 响应错误码
        """
        try:
            if 500 <= status_code <= 504:
                exc_info = kwargs.get('exc_info')
                subject = getattr(config, 'subject', '邮件报警提醒')
                msg = "".join(traceback.format_exception(*exc_info))
                # 可选的邮件通知
                # TODO: 当前模式下 autoreload 时因为没有显示关闭进程池, 会导致端口被占用而无法重启, 暂时关闭邮件通知
                # pool = self.__class__.TASK_POOL
                # pool.submit(alarm.sentLog, subject, msg)
        except Exception:
            msg = traceback.format_exc()
            logger.warning(msg)
        super().write_error(status_code, **kwargs)

    async def get(self, *args, **kwargs):
        """
        测试服务是否正在运行
        """
        return self.write('It works')


class MainHandler(BaseHandler):
    """
    渲染测试页面
    """

    async def get(self, *args, **kwargs):
        return self.render('templates/demo.html',
                           hostname=config.host,
                           port=config.default_port)


class WebhookHandler(BaseHandler):
    """
    吾来平台消息路由处理
    """

    async def post(self, *args, **kwargs):
        logger.info(' in WebhookHandler  '.center(80, '-'))
        params = self.body or json.loads(self.request.body)
        logger.info(params)
        resp = {
            'suggested_response': params['bot_response']['suggested_response']
        }
        try:
            resp['suggested_response'] = webhook.get_reply(params)
        except Exception:
            msg = traceback.format_exc()
            logger.error(msg)
        resp.setdefault('suggested_response', {})
        logger.info(resp)
        logger.info(' out WebhookHandler  '.center(80, '-'))
        self.write(resp)


class CallbackHandler(tornado.web.RequestHandler):
    """
    接收吾来平台回调, 返回信息给客户端
    """

    async def post(self, *args, **kwargs):
        data = json.loads(self.request.body)
        logger.debug('===================callback======================')
        logger.debug(data)
        if 'text' in data['msg_body']:
            content = data['msg_body']['text'].get('Content', '未能识别, 请稍后尝试')
            user_id = data['user_id']
            await UserSocketHandler.ai_reply(content, user_id)
        self.write(json.dumps({}))


class UserSocketHandler(tornado.websocket.WebSocketHandler):
    """
    客户端长连接, 处理接收和发送消息
    """

    users = {}  # 维护一个所有用户的字典，以便后面根据吾来回调返回回复内容

    def __init__(self, application, request, **kwargs):
        super(UserSocketHandler, self).__init__(application, request, **kwargs)
        self.user_id = None

    def check_origin(self, origin):
        return True

    async def open(self):
        """
        新建连接
        """
        logger.debug('New user log in ')

    async def on_message(self, data):
        """
        接受消息
        """
        logger.debug('Received new message %s' % data)
        data = json.loads(data)
        if data['action'] == 'login':
            res = await self.handle_login(data)
            await self.write_message(res)
        else:
            await self.handle_query(data)

    async def get_history(self, user_id):
        """
        获取吾来用户历史对话记录
        """
        history = []
        res = await ai_client.get_msg_history(user_id)
        if res:
            logger.info(f"{res}")
            msg_li = res.get('items', [])
            if msg_li:
                for item in msg_li:
                    data = {
                        'is_self': False,
                        'time': int(item['msg_ts'])
                    }
                    if item['direction'] == 'USER_SEND':
                        data['is_self'] = True
                    if 'text' in item['body']:
                        content = item['body']['text']['content']
                        data['msg'] = content
                        history.append(data)
            else:
                logger.warning(f"userid: {user_id}, {res}")
        return history

    async def handle_login(self, data):
        """
        处理登录信息
        """
        user_id = data['user_id']
        await ai_client.create_user(user_id, user_id)

        UserSocketHandler.users[user_id] = self
        userinfo = data['user_info']
        if not isinstance(userinfo, dict):
            userinfo = dict()
        x_real_ip = self.request.headers.get("X-Real-IP")
        remote_ip = x_real_ip or self.request.remote_ip

        logger.info("x_real_ip: %s" % x_real_ip)
        logger.info("remote_ip: %s" % remote_ip)

        userinfo.setdefault('campaign', {})
        userinfo['campaign']['ip_address'] = remote_ip

        if "User-Agent" not in self.request.headers:
            device = data.get('device')
        else:
            user_agent = parse(self.request.headers["User-Agent"])
            device = user_agent.os.family.lower()
        channel_info = {"ios": "app_ios", "android": 'app_android'}
        userinfo['source'] = channel_info.get(device, 'website_cn')

        result = {
            'action': 'login',
            'ok': True,
            'history': await self.get_history(user_id),
        }
        return json.dumps(result)

    async def handle_query(self, data):
        """
        转发客户端消息给吾来平台
        """
        msg = data['msg']
        user_id = data['user_id']
        await ai_client.send_user_msg(msg, user_id)

    @classmethod
    async def ai_reply(cls, data, user_id):
        """
        根据用户 ID 发送吾来回复消息给客户端
        """
        logger.debug('ai_reply: %s to %s' % (data, user_id))
        if user_id in cls.users:
            logger.debug("send reply")
            result = {
                'action': 'reply',
                'data': data
            }
            await cls.users[user_id].write_message(result)
        else:
            logger.debug("user not found")

    async def on_close(self):
        """
        连接关闭, 清理资源
        """
        logger.debug("socket closed, cleaning up resources now")
        if self.user_id in UserSocketHandler.users:
            del UserSocketHandler.users[self.user_id]


class TestHandler(BaseHandler):

    async def get(self, *args, **kwargs):
        wait_time = random.randint(100, 2000) / 1000.0
        await asyncio.sleep(wait_time)
        msg = ''
        msg += 'test' * 10
        self.write({'msg': msg})

    async def post(self, *args, **kwargs):
        wait_time = random.randint(100, 2000) / 1000.0
        await asyncio.sleep(wait_time)
        msg = ''
        msg += 'test' * 10
        self.write({'msg': msg})


if __name__ == '__main__':
    pass

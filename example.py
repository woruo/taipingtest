import asyncio
import traceback
import aiohttp


async def async_request():
    """
    发送一个异步请求
    """
    async with aiohttp.ClientSession() as session:
        async with session.get('http://www.baidu.com') as resp:
            text = await resp.text()
            print(text)


def main():
    asyncio.get_event_loop().run_until_complete(async_request())


if __name__ == '__main__':
    main()

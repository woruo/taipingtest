import time
import traceback
from contextlib import contextmanager

import redis
try:
    import wulai_logger as logger
except ImportError:
    import logging as logger


def get_entity(entities, entity_name, *, is_value=False):
    """
    获取实体引用
    :param entities: 实体列表
    :param entity_name: 实体名称
    :param is_value: 是否返回实体值， False 时返回实体引用
    """
    try:
        for entity in entities:
            if entity_name == entity["name"]:
                return entity['value'] if is_value else entity
    except Exception:
        msg = traceback.format_exc()
        logger.warning(msg)
    return None


def modify_entity(entities, name, value='', seg_value=None):
    """
    将实体列表里的值全部修改为指定的值, 默认传值表示删除该实体
    :param entities: 实体列表
    :param name: 需要修改的实体名字
    :param value: 实体修改后的值
    :param seg_value: 实体抽取的真实文本
    """
    if seg_value is None:
        seg_value = value
    for entity in entities:
        if entity['name'] == name:
            entity['value'] = value
            entity['seg_value'] = seg_value


def kill_task(entities):
    """
    提前终止任务
    :param entities: 实体列表
    """
    ignored = {"app_id", "task_id"}
    return [entity for entity in entities if entity['name'] in ignored]


def set_next_text_reply(one_response, content, show_content=None):
    """
    修改候选回复的文本
    :param one_response: 建议候选回复中的一个
    :param content: 回复内容的文本值
    :param show content: 回复内容的文本显示值
    """
    if show_content is None:
        show_content = content
    res = one_response['response'][0]
    res['msg_body']['text']['content'] = content
    res['show_content'] = content


def delete_entities(entities, *args):
    """
    删除指定的实体值
    :param entities: 实体列表
    :param args: 需要删除的实体列表
    """
    for entity in entities:
        if entity['name'] in args:
            entity['value'] = ''
            entity['seg_value'] = ''
    return entities


def reset_task(entities, *, last_entity_name=''):
    """
    重置任务, 回到第一个对话单元
    :param entities: list 实体列表
    :param last_entity_name: 首个对话单元的实体名称
    """
    fields = {'app_id', 'task_id', 'interval', 'last_block_id',
              'last_entity_name', 'original', 'last_story', }
    for e in entities:
        if e['name'] not in fields:
            e['value'] = ''
            e['seg_value'] = ''
    last_entitiy_name = get_entity(entities, 'last_entity_name')
    last_entitiy_name['value'] = last_entitiy_name
    last_entitiy_name['seg_value'] = last_entitiy_name


def is_from_wechat_robot(params):
    """
    判断是否来自群机器人渠道
    :param params: webhook 传入参数
    """
    return params['group_info']['from_group_user'] is not None


def get_image_reply(img_url, delay_ts=0, *, thumb_url='', **kwargs):
    return {
        "delay_ts": delay_ts,
        "handle_routine": None,
        "msg_body": {
            "extra": "",
            "image": {
                "resource_url": img_url,
                "thumb_url": ""
            }
        },
        "show_content": img_url,
    }


class IntervalCount:
    """
    用于对话单元计数
    """

    REDIS_CLI = None
    REDIS_HOST = ''
    REDIS_PORT = ''
    REDIS_PWD = ''
    REDIS_DB = ''
    PREFIX = ''
    EXPIRE = 0

    def __init__(self, *args, **kwargs):
        pass

    @property
    def redis_cli(self):
        cli = self.REDIS_CLI
        if cli:
            if not cli.ping():
                cli = None
                self.REDIS_CLI = None
        if not cli:
            cli = redis.StrictRedis(host=self.REDIS_HOST,
                                    port=self.REDIS_PORT,
                                    password=self.REDIS_PWD,
                                    ssl=False, decode_responses=True)
            self.REDIS_CLI = cli
        return cli

    def init_block(self, user_id, task_id):
        cli = self.redis_cli
        name = f"{self.PREFIX}_{user_id}_{task_id}"
        cli.hsetnx(name, user_id, task_id)
        if cli.ttl(name) != -2:
            cli.expire(name, self.EXPIRE)

    def incr_block_count(self, user_id, task_id, block):
        cli = self.redis_cli
        name = f"{self.PREFIX}_{user_id}_{task_id}"
        cli.expire(name, self.EXPIRE)
        return cli.hincrby(name, block)

    def get_block_count(self, user_id, task_id, block):
        cli = self.redis_cli
        name = f"{self.PREFIX}_{user_id}_{task_id}"
        res = cli.hget(name, block)
        return int(res) if res else 0

    def set_block_count(self, user_id, task_id, block, count):
        cli = self.redis_cli
        name = f"{self.PREFIX}_{user_id}_{task_id}"
        return cli.hset(name, block, count)

    def clear_block(self, user_id, task_id):
        cli = self.redis_cli
        name = f"{self.PREFIX}_{user_id}_{task_id}"
        cli.delete(name)


@contextmanager
def timeblock(label):
    """
    显示代码运行消费时间
    """
    start = time.time()
    try:
        yield
    finally:
        end = time.time()
        logger.info('{} cost: {:6f}'.format(label, end - start))

if __name__ == '__main__':
    pass
    with timeblock('test'):
        pass


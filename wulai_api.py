# -*- coding: UTF-8 -*-
import time
import uuid
import hashlib
import requests
import json
from wulai_logger import logger
from utils import CONFIG


def get_headers():
    pubkey = CONFIG.get("channel", "KEY")
    secret = CONFIG.get("channel", "SECRET")
    # 秒级别时间戳
    timestamp = str(int(time.time()))
    nonce = uuid.uuid4().hex
    sign = hashlib.sha1((nonce + timestamp + secret).encode("utf-8")).hexdigest()
    data = {
        "pubkey": pubkey,
        "sign": sign,
        "nonce": nonce,
        "timestamp": timestamp
    }
    headers = {}
    for k, v in data.items():
        headers["Api-Auth-" + k] = v
    return headers


def send_request(url, data):
    res = requests.post(url,
                        data=json.dumps(data),
                        headers=get_headers()
                        )
    result = ''
    try:
        result = json.loads(res.content)
    except Exception as e:
        logger.error(res.content)
    return result


def create_user(nickname, username, img_url=''):
    """
    创建用户
    :param nickname:
    :param username:
    :param img_url:
    :return:
    """
    user_data = {
        "imgurl": img_url,
        "nickname": nickname,
        "username": username
    }
    url = CONFIG.get("channel", "HOST") + "/v1/user/create"
    return send_request(url, user_data)


def receive_user_msg(data, user_id):
    """
    接收用户消息
    :param data:
    :param user_id:
    :return:
    """
    data = {
        "msg_body": {
            "text": {
                "content": data,
                "tts": "",
                "tts_type": "AMR"
            },

        },
        "third_msg_id": "",
        "user_id": user_id
    }
    url = CONFIG.get("channel", "HOST") + "/v1/msg/receive"
    return send_request(url, data)


def get_msg_history(user_id):
    """
    获取消息历史
    :param user_id:
    :return:
    """
    post_data = {
        "direction": "BACKWARD",
        "msg_id": "",
        "num": 20,
        "user_id": user_id
    }
    url = CONFIG.get("channel", "HOST") + "/v1/msg/history"
    return send_request(url, post_data)


def send_user_msg_directly(data, user_id):
    """
    直接向用户发送消息
    :param data:
    :param user_id:
    :return:
    """
    data = {
        "msg_body": {
            "text": {
                "content": data,
                "tts": "",
                "tts_type": "AMR"
            },

        },
        "send_ts": str(int(time.time())),
        "user_id": user_id,
        "at_group_user_id": []
    }
    url = CONFIG.get("channel", "HOST") + "/v1/msg/send"
    return send_request(url, data)

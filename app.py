# -*- coding: UTF-8 -*-
import sys
import asyncio
import tornado
import tornado.web
import tornado.ioloop
import config
import handle

try:
    import wulai_logger as logger
except ImportError:
    import logging as logger


def make_app():
    settings = {
        "debug": config.debug,
        'autoreload': config.autoreload,
        'static_path': './static',

    }
    return tornado.web.Application([
        (r'/', handle.MainHandler),
        (r'/v1/chat', handle.UserSocketHandler),
        (r'/v1/callback', handle.CallbackHandler),
        (r'/v1/webhook', handle.WebhookHandler),
        (r'/v1/test', handle.TestHandler),
    ], **settings)


async def loop_task():
    """
    后台任务队列
    """
    wait_time = 600
    while True:
        await asyncio.sleep(wait_time)
        # Todo: do something


if __name__ == "__main__":
    port = sys.argv[1] if len(sys.argv) >= 2 else config.default_port
    app = make_app()
    server = tornado.httpserver.HTTPServer(app)

    server.bind(port)
    server.start(config.mulit_process_num)

    logger.info(f'listening on {port}')
    tornado.ioloop.IOLoop.current().spawn_callback(loop_task)
    tornado.ioloop.IOLoop.current().start()

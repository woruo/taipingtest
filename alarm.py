import os
import smtplib
import traceback
from email.mime.text import MIMEText
from email.header import Header
import config

try:
    import wulai_logger as logger
except ImportError:
    import logging as logger


def sentLog(subject, content, *, receiver=None, from_msg='notify@laiye.com'):
    """
    邮件发送
    :param subject: 邮件主题
    :param content: 邮件内容
    :param receiver: 邮件接收, str or list
    :param from_msg: 发件人信息
    """
    # 第三方 SMTP 服务
    mail_host = os.getenv('mail_host', "smtp.exmail.qq.com")  # 设置服务器
    mail_user = os.getenv('mail_user', 'notify@laiye.com')  # 用户名
    mail_pass = os.getenv('mail_pass', 'Laiye@2015')  # 口令

    # 接收邮件，可设置为你的QQ邮箱或者其他邮箱
    if receiver is None and config.EMAIL_RECEIVER:
        receiver = config.EMAIL_RECEIVER

    assert receiver is not None, '无接受者'

    if isinstance(receiver, str):
        receivers = [receiver]
    else:
        receivers = receiver

    # 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
    message = MIMEText(content, 'plain', 'utf-8')

    message['From'] = Header(from_msg, 'utf-8')  # 发送者
    message['To'] = Header(", ".join(receivers), 'utf-8')  # 接收者

    subject = subject
    message['Subject'] = Header(subject, 'utf-8')

    try:
        smtpObj = smtplib.SMTP_SSL(mail_host)
        smtpObj.login(mail_user, mail_pass)
        smtpObj.sendmail(from_msg, receivers, message.as_string())
        logger.info("[邮件发送成功]")
    except Exception:
        err = traceback.format_exc()
        logger.warning(err)


if __name__ == '__main__':
    receiver = 'wangqisheng@laiye.com'
    receiver = ['wangqisheng@laiye.com']
    receiver = ['wqs1639@gmail.com', 'wangqisheng@laiye.com']
    sentLog('测试', '测试', receiver=receiver)

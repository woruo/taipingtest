import os
import logging

log_level = os.getenv('log_level', logging.DEBUG)

debug = os.getenv('proj') != 'prod'

normal_log_path = os.getenv('normal_log_path', 'logs/runing.log')

warn_log_path = os.getenv('warn_log_path', 'logs/warn.log')

error_log_path = os.getenv('error_log_path', 'logs/error.log')

EMAIL_RECEIVER = os.getenv('receiver', 'chenworuo@laiye.com')

wul_api_host = os.getenv('wul_api_host', 'https://openapi.wul.ai')

app_key = os.getenv('app_key', 'F0Jg9jEFyIxlqaW2UYj998WwkzxjE61C0050070029ebbaca79')

app_secret = os.getenv('app_secret', 'uct2siwmfGb9zCrWEtwj')

host = os.getenv('host', "139.219.128.71")

default_port = os.getenv('port', 8868)

autoreload = bool(int(os.getenv('AUTORELOAD', 0)))

mulit_process_num = int(os.getenv('mulit_process_num', 1))

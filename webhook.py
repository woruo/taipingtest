# -*- coding: UTF-8 -*-
import utils

try:
    import wulai_logger as logger
except ImportError:
    import logging as logger


def get_reply(params):
    """
    处理 webhook 候选回复， 返回给吾来平台
    :param params: 吾来平台消息路由传入参数
    """
    logger.info("get_reply start".center(80, '-'))
    # 获取基本信息
    query = params["msg_body"]['text']['content']

    for one_response in params["bot_response"]["suggested_response"]:
        try:
            source = one_response["source"]
        except Exception:
            source = "DEFAULT_ANSWER_SOURCE"
            logger.info("无法获取任何回复，将进行兜底回复")
        if source == "DEFAULT_ANSWER_SOURCE":
            logger.info("无法召回任何机器人和知识点，将进行兜底回复")
        elif source == "TASK_BOT":
            # 是机器人
            logger.info("召回任务机器人")

            # 获取实体列表引用和action的值
            task = one_response['detail']['task']
            entities = task['entities']
            action = task['action']
            logger.info('动作:\t%s' % action)
            if query.strip() in {"退出", "清空", "结束", 'quit', 'exit'}:
                # 提前结束任务
                entities = utils.kill_task(entities)
                task['state'] = 1
                task['entities'] = entities
                text = '感谢使用， 再见！'
                utils.set_next_reply(one_response, text)
                continue

            # 根据对话单元别名进行逻辑处理
            if action == '':
                pass

            deleted = ['sys.date', 'sys.num']
            entities = utils.delete_entities(entities, *deleted)
            task['entities'] = entities

        else:
            # 召回问答机器人或者关键词机器人
            logger.info("召回问答机器人或者关键词机器人，将不做处理")

    logger.info('[get_reply]:\t文本处理结束')
    return params["bot_response"]["suggested_response"]
